# DBSCAN

C++ implementation of different DBSCAN Algorithms in the two-dimensional euclidian space.

- Original DBSCAN
- Faster DBSCAN by Ade Gunawan (https://arxiv.org/abs/1702.08607)

## Dependencies

- [boost](https://www.boost.org/)
- [robin-hood-hashing](https://github.com/martinus/robin-hood-hashing)

## Instructions

### Build
```sh
make
```

### Usage
```
Usage: dbscan <eps> <minpts> [options]
Options:
  --help                Show help message
  --eps arg             epsilon parameter
  --minpts arg          minPts parameter
  -o [ --original ]     Original DBSCAN algorithm
  -f [ --faster ]       Faster DBSCAN algorithm
  -n [ --nonregular ]   Nonregular grid
  -r [ --regular ]      Regular grid
  -t [ --r-tree ]       R*-Tree (only used with -o)
  --benchmark arg       Benchmark mode
```

Points are read by standard input, the DBSCAN result is printed to standard output.

One of the following modes for `dbscan` must be specified:
- `-o`: Original DBSCAN
- `-on`: Original DBSCAN with nonregular grid
- `-or`: Original DBSCAN with regular grid
- `-ot`: Original DBSCAN with R\*-Tree
- `-fn`: Faster DBSCAN with nonregular grid
- `-fr`: Faster DBSCAN with regular grid

#### Example
```sh
./random 10000 --ratio 0.2 | ./dbscan 0.92 8 -fn
```
