#include <iostream>
#include <random>

// https://www.boost.org/
#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include "dbscan.hh"

// initialize random device
std::random_device rd;
std::mt19937_64 gen(rd());

template <class InsertIterator>
void generate_box(unsigned long n, double x_min, double x_max, double y_min,
                  double y_max, InsertIterator it, double shake = 0) {
    std::uniform_real_distribution off(-shake, shake);
    std::uniform_real_distribution xdist(x_min, x_max), ydist(y_min, y_max);
    for (unsigned int i = 0; i < n; i++) {
        Point p((CoordinateType)(xdist(gen) + off(gen)),
                (CoordinateType)(ydist(gen) + off(gen)));
        it = p;
    }
}

template <class InsertIterator>
void generate_circle(unsigned long n, double x_center, double y_center,
                     double radius, InsertIterator it) {
    std::uniform_real_distribution xdist(x_center - radius, x_center + radius),
        ydist(y_center - radius, y_center + radius);
    Point center(x_center, y_center);
    unsigned int i = 0;
    while (i < n) {
        Point p((CoordinateType)(xdist(gen)), (CoordinateType)(ydist(gen)));
        if (bg::distance(center, p) <= radius) {
            it = p;
            i++;
        }
    }
}

int main(int ac, char **av) {
    try {
        auto usage = "Usage: random <n> [options]";
        po::options_description desc("Options");
        desc.add_options()("help", "Show help message")(
            "n", po::value<unsigned long>(),
            "Number of points")("ratio", po::value<double>(), "Ratio of noise")(
            "factor", po::value<unsigned int>(), "Factor");

        po::positional_options_description p;
        p.add("n", 1);

        po::variables_map vm;
        po::store(
            po::command_line_parser(ac, av).options(desc).positional(p).run(),
            vm);
        po::notify(vm);

        if (vm.count("help") || !(vm.count("n") && vm.count("ratio"))) {
            std::cout << usage << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        const unsigned long n = vm["n"].as<unsigned long>();
        unsigned long xy_max = (unsigned long)sqrt((double)n);

        const double ratio = vm["ratio"].as<double>();
        unsigned int factor = 1;
        if (vm.count("factor"))
            factor = vm["factor"].as<unsigned int>();

        std::vector<Point> points;

        for (unsigned int k = 0; k < factor; k++) {
            auto off = k * xy_max;
            generate_box(n * ratio, 0 + off, xy_max + off, 0, xy_max,
                         std::back_inserter(points));
            generate_circle(n * (1 - ratio), (0.5*xy_max) + off, (0.5*xy_max), 0.25*xy_max,
                         std::back_inserter(points));
        }

        for (Point &p : points) {
            std::cout << p.x() << " " << p.y() << std::endl;
        }
        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
