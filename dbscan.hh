// DBSCAN

#include <algorithm>
#include <iostream>
#include <map>
#include <stack>
#include <vector>

// https://www.boost.org/
#include <boost/container/small_vector.hpp>
#include <boost/geometry.hpp>
#include <boost/range/adaptor/transformed.hpp>

// https://github.com/martinus/robin-hood-hashing
#include "robin_hood.h"

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

using CoordinateType = double;
using Point = bg::model::d2::point_xy<CoordinateType>;
using Box = bg::model::box<Point>;
using Segment = bg::model::segment<Point>;

typedef long Id;

enum Cluster : Id {
    UNCLASSIFIED = -1,
    NOISE = 0,
};

enum PointType {
    TYPE_CORE_POINT,
    TYPE_BORDER_POINT,
    TYPE_NOISE,
};

struct PointData {
    bool is_core;
    Id cluster;

    PointData(bool is_core, Id cluster) : is_core(is_core), cluster(cluster) {}
    PointData() : is_core(false), cluster(UNCLASSIFIED) {}

    PointType type() {
        if (is_core) {
            return TYPE_CORE_POINT;
        } else {
            if (cluster) {
                return TYPE_BORDER_POINT;
            } else {
                return TYPE_NOISE;
            }
        }
    }
};

// Nonregular grid with cells of maximum diameter `eps`
class NonregularGrid {
  public:
    // Cell containing points
    class Cell {
      public:
        typedef std::vector<const Point *>::const_iterator iterator;

      private:
        iterator m_it_begin;
        iterator m_it_end;

      public:
        class Bounds {
            Box m_box;

          public:
            Bounds(Box box) : m_box(box){};

            const Box box() const { return m_box; }

            CoordinateType x_min() const { return m_box.min_corner().x(); }
            CoordinateType y_min() const { return m_box.min_corner().y(); }
            CoordinateType x_max() const { return m_box.max_corner().x(); }
            CoordinateType y_max() const { return m_box.max_corner().y(); }
        } bounds;

        std::vector<const Cell *> m_neighbors;

        Cell(iterator begin, iterator end, Box box)
            : m_it_begin(begin), m_it_end(end), bounds(box) {
            m_neighbors.reserve(20);
        }

        const iterator begin() const { return m_it_begin; }
        const iterator end() const { return m_it_end; }

        // Returns number of points in cell
        size_t size() const { return end() - begin(); }
    };

    // Strip containing cells
    class Strip {
      public:
        typedef std::vector<Cell>::iterator iterator;

      private:
        union {
            size_t m_index_begin;
            iterator m_it_begin;
        };
        union {
            size_t m_index_end;
            iterator m_it_end;
        };

      public:
        class Bounds {
            CoordinateType m_x_min, m_x_max;

          public:
            Bounds(CoordinateType x_min, CoordinateType x_max)
                : m_x_min(x_min), m_x_max(x_max) {}

            CoordinateType x_min() const { return m_x_min; }
            CoordinateType x_max() const { return m_x_max; }
        } bounds;

        Strip(size_t begin, size_t end, Bounds bounds)
            : m_index_begin(begin), m_index_end(end), bounds(bounds) {}

        void create_iterators(iterator &&it_begin) {
            m_it_begin = it_begin + m_index_begin;
            m_it_end = it_begin + m_index_end;
        }

        iterator begin() const { return m_it_begin; }
        iterator end() const { return m_it_end; }
    };

  private:
    std::vector<const Point *> m_points;
    std::vector<Cell> m_cells;
    std::vector<Strip> m_strips;

    // Strip range with maximum distance of 2 * `y_dist`
    struct StripRange {
        typedef std::vector<Cell>::iterator iterator;

        const Strip &strip;
        const double y_dist;
        iterator begin;
        iterator end;

        StripRange(const Strip &strip, const double y_dist)
            : strip(strip), y_dist(y_dist), begin(strip.begin()),
              end(strip.begin()) {}
    };

  public:
    typedef std::vector<Cell>::const_iterator iterator;

    // Creates nonregular grid from a set of points
    NonregularGrid(const std::vector<Point> &points, const double eps) {
        const double cell_width = M_SQRT1_2 * eps;

        // Create grid container
        size_t n = points.size();
        m_points = std::vector<const Point *>(n);
        for (size_t i = 0; i < n; i++) {
            this->m_points[i] = &points[i];
        }

        // Sort points in strips and cells
        size_t strip_begin_cell_index = 0;
        // Sort grid by x-coordinate
        std::sort(
            m_points.begin(), m_points.end(),
            [](const Point *p, const Point *q) { return p->x() < q->x(); });
        // Partition grid into strips of maximum width `cell_width`
        for (auto strip_begin = m_points.begin();
             strip_begin != m_points.end();) {
            auto strip_end = std::partition_point(
                strip_begin, m_points.end(),
                [strip_begin, cell_width](const Point *p) {
                    return p->x() - (*strip_begin)->x() <= cell_width;
                });
            // Get bounds of strip
            Strip::Bounds strip_bounds(strip_begin[0]->x(), strip_end[-1]->x());
            // Sort strip by y-coordinate
            std::sort(
                strip_begin, strip_end,
                [](const Point *p, const Point *q) { return p->y() < q->y(); });
            // Partition strip into cells of maximum width `cell_width`
            for (auto cell_begin = strip_begin; cell_begin != strip_end;) {
                auto cell_end = std::partition_point(
                    cell_begin, strip_end,
                    [cell_begin, cell_width](const Point *p) {
                        return p->y() - (*cell_begin)->y() <= cell_width;
                    });
                // Get bounds of cell
                auto [x_min_elem, x_max_elem] = std::minmax_element(
                    cell_begin, cell_end, [](const Point *p, const Point *q) {
                        return p->x() < q->x();
                    });
                Box cell_bounds{{(*x_min_elem)->x(), cell_begin[0]->y()},
                                {(*x_max_elem)->x(), cell_end[-1]->y()}};
                // Add new cell
                m_cells.push_back(Cell(cell_begin, cell_end, cell_bounds));
                // Next cell
                cell_begin = cell_end;
            }
            // Add new strip using indices because iterators may invalidate
            size_t strip_end_cell_index = m_cells.size();
            m_strips.push_back(Strip(strip_begin_cell_index,
                                     strip_end_cell_index, strip_bounds));
            strip_begin_cell_index = strip_end_cell_index;
            // Next strip
            strip_begin = strip_end;
        }
        // Convert indices to pointers
        for (Strip &s : m_strips) {
            s.create_iterators(m_cells.begin());
        }

        // Iterate through strips and connect neighboring cells
        for (size_t i = 0; i < m_strips.size(); i++) {
            Strip &s = m_strips[i];
            // Initialize neighbor strips
            boost::container::small_vector<StripRange, 2> neighbor_strips_range;
            for (int j = 1; j <= 2; j++) {
                // Check if neighbor strip exists
                if ((int)i - j < 0) {
                    break;
                }
                Strip &ns = m_strips[i - j];
                double x_dist = s.bounds.x_min() - ns.bounds.x_max();
                double y_dist = sqrt(eps * eps - x_dist * x_dist);
                // Check if neighbor strip is reachable from strip
                if (!(y_dist >= 0)) {
                    break;
                }
                // Add neighbor strip
                neighbor_strips_range.push_back(StripRange(ns, y_dist));
            }
            // Iterate through cells in strip
            Strip::iterator s_begin = s.begin();
            for (Strip::iterator c = s.begin(); c != s.end(); c++) {
                // Process this strip
                while (s_begin->bounds.y_max() - c->bounds.y_min() < -eps) {
                    s_begin++;
                }
                for (Strip::iterator nc = s_begin; nc != c; nc++) {
                    // Connect c with nc
                    c->m_neighbors.push_back(&*nc);
                    nc->m_neighbors.push_back(&*c);
                }
                // Process neighbor strips
                for (StripRange &ns : neighbor_strips_range) {
                    while (ns.begin != ns.strip.end() &&
                           ns.begin->bounds.y_max() - c->bounds.y_min() <
                               -ns.y_dist) {
                        ns.begin++;
                    }
                    while (ns.end != ns.strip.end() &&
                           ns.end->bounds.y_min() - c->bounds.y_max() <=
                               ns.y_dist) {
                        ns.end++;
                    }
                    for (Strip::iterator nc = ns.begin; nc != ns.end; nc++) {
                        if (bg::distance(c->bounds.box(), nc->bounds.box()) <=
                            eps) {
                            // Connect c with nc
                            c->m_neighbors.push_back(&*nc);
                            nc->m_neighbors.push_back(&*c);
                        }
                    }
                }
            }
        }
    }

    const std::vector<Cell> &cells() const { return m_cells; }

    const iterator begin() const { return m_cells.begin(); }
    const iterator end() const { return m_cells.end(); }

    // Returns neighboring cells excluding itself
    const std::vector<const Cell *> &
    neighboring_cells(const NonregularGrid::Cell &c) const {
        return c.m_neighbors;
    }

    // Returns grid cell which could contain `p`.
    // If `p` is outside the grid, throws exception.
    // Runtime complexity of O(log N)
    const Cell &get_cell(const Point &p) const {
        auto it_strip =
            std::lower_bound(m_strips.begin(), m_strips.end(), p.x(),
                             [](const Strip &strip, const CoordinateType x) {
                                 return strip.bounds.x_max() < x;
                             });
        if (it_strip == m_strips.end() || it_strip->bounds.x_min() > p.x()) {
            throw std::invalid_argument("Point outside grid");
        }
        auto it_cell =
            std::lower_bound(it_strip->begin(), it_strip->end(), p.y(),
                             [](const Cell &cell, const CoordinateType y) {
                                 return cell.bounds.y_max() < y;
                             });
        if (it_cell == it_strip->end() || it_cell->bounds.y_min() > p.y()) {
            throw std::invalid_argument("Point outside grid");
        }
        return *it_cell;
    }
};

// Regular grid with cells of exact diameter `eps`
class RegularGrid {
  public:
    class Cell {
        std::vector<const Point *> m_points;

      public:
        typedef std::vector<const Point *>::const_iterator iterator;

        std::vector<const Cell *> m_neighbors;

        const long col, row;
        Cell(const long col, const long row) : col(col), row(row) {
            m_neighbors.reserve(20);
        };

        void add(const Point *p) { m_points.push_back(p); }

        const iterator begin() const { return m_points.begin(); }
        const iterator end() const { return m_points.end(); }

        size_t size() const { return m_points.size(); }
    };

  private:
    CoordinateType x_min, x_max, y_min, y_max;
    double cell_width;
    long n_cols, n_rows;

    robin_hood::unordered_flat_map<long, size_t> m_grid;
    std::vector<Cell> m_cells;

  public:
    typedef typename std::vector<Cell>::const_iterator iterator;

    // Creates regular grid from a set of points.
    RegularGrid(const std::vector<Point> &points, const double eps) {
        cell_width = M_SQRT1_2 * eps;

        // Initialize bounds
        {
            auto it = points.begin();
            x_min = it->x();
            x_max = it->x();
            y_min = it->y();
            y_max = it->y();
            while (++it != points.end()) {
                if (it->x() < x_min)
                    x_min = it->x();
                if (it->x() > x_max)
                    x_max = it->x();
                if (it->y() < y_min)
                    y_min = it->y();
                if (it->y() > y_max)
                    y_max = it->y();
            }
        }

        // Compute number of rows and columns
        n_cols = (long)floor((x_max - x_min) / cell_width) + 1;
        n_rows = (long)floor((x_max - x_min) / cell_width) + 1;

        size_t n_points = points.size();
        size_t n_cells = n_cols * n_rows;
        size_t n = std::min(n_points, n_cells);

        m_grid.reserve(n);
        m_cells.reserve(n);

        // Add points to grid
        for (const Point &p : points) {
            long col = floor((p.x() - x_min) / cell_width);
            long row = floor((p.y() - y_min) / cell_width);
            insert_cell(col, row).add(&p);
        }

        // Compute neighboring cells
        const long neighbors_offset[20][2] = {
            {0, 1},   {1, 0},  {0, -1},  {-1, 0},  {1, 1},  {1, -1}, {-1, 1},
            {-1, -1}, {0, 2},  {2, 0},   {0, -2},  {-2, 0}, {2, 1},  {1, 2},
            {-1, 2},  {-2, 1}, {-2, -1}, {-1, -2}, {1, -2}, {2, -1}};
        for (Cell &c : m_cells) {
            for (auto &[i, j] : neighbors_offset) {
                long col = c.col + i, row = c.row + j;
                if (col >= 0 && col < n_cols && row >= 0 && row < n_rows) {
                    long key = col + n_cols * row;
                    auto it = m_grid.find(key);
                    if (it != m_grid.end()) {
                        c.m_neighbors.push_back(&m_cells[it->second]);
                    }
                }
            }
        }
    }

    // Returns grid cell which could contain `p`.
    // If `p` is outside the grid, throws exception.
    // Runtime complexity of O(N)
    const Cell &get_cell(const Point &p) const {
        long col = floor((p.x() - x_min) / cell_width);
        long row = floor((p.y() - y_min) / cell_width);
        long key = col + n_cols * row;
        auto it = m_grid.find(key);
        if (it != m_grid.end()) {
            return m_cells[it->second];
        } else {
            throw std::invalid_argument("Point outside grid");
        }
    }

    // Returns grid cell at (col, row).
    // If cell does not exist, it will be created.
    Cell &insert_cell(const long col, const long row) {
        long key = col + n_cols * row;
        auto [it, success] = m_grid.insert({key, m_cells.size()});
        if (success) {
            m_cells.push_back(Cell(col, row));
            return m_cells.back();
        } else {
            return m_cells[it->second];
        }
    }

    // Returns neighboring cells excluding itself.
    const std::vector<const Cell *> &neighboring_cells(const Cell &c) const {
        return c.m_neighbors;
    }

    const iterator begin() const { return m_cells.begin(); }
    const iterator end() const { return m_cells.end(); }

    const std::vector<Cell> &cells() const { return m_cells; }
};

struct Result {
    std::vector<PointData> data;

    // verify result
    bool verify() {
        for (PointData &p : data) {
            if (p.cluster < 0) {
                return false;
            }
            if (p.cluster == 0 && p.is_core) {
                return false;
            }
        }
        return true;
    }

    // compare results
    bool operator==(Result &other) {
        size_t n = data.size();
        if (other.data.size() != n) {
            return false;
        }
        // map cluster ids
        std::map<Id, Id> map;
        for (size_t i = 0; i < n; i++) {
            PointData &p = data[i], &q = other.data[i];
            // compare core points
            if (p.is_core != q.is_core) {
                return false;
            }
            // compare noise
            if (!p.cluster != !q.cluster) {
                return false;
            }
            // only compare cluster of core points
            if (!p.is_core) {
                continue;
            }
            // compare cluster
            auto [it, success] = map.insert({p.cluster, q.cluster});
            if (!success && it->second != q.cluster) {
                return false;
            }
        }
        return true;
    }
};

class DBSCAN {
  protected:
    const double eps;
    const unsigned int minpts;

  public:
    DBSCAN(const double eps, const unsigned int minpts)
        : eps(eps), minpts(minpts) {}

    Result operator()(const std::vector<Point> &points);
};

template <class Grid> class GridRangeQuery {
    const Grid grid;
    const double eps;

  public:
    GridRangeQuery(const std::vector<Point> &points, const double eps)
        : grid(points, eps), eps(eps) {}

    template <class InsertIterator>
    void operator()(const Point &p, InsertIterator it) const {
        const typename Grid::Cell &c = grid.get_cell(p);
        for (const Point *q : c) {
            if (bg::distance(p, q) <= eps) {
                it = q;
            }
        }
        for (const typename Grid::Cell *nc : grid.neighboring_cells(c)) {
            for (const Point *q : *nc) {
                if (bg::distance(p, q) <= eps) {
                    it = q;
                }
            }
        }
    }
};

class RTreeRangeQuery {
    typedef std::pair<Point, size_t> Value;
    typedef bgi::rtree<Value, bgi::rstar<16>> RTree;

    const std::vector<Point> &points;
    const double eps;

    RTree rtree;

  public:
    RTreeRangeQuery(const std::vector<Point> &points, const double eps)
        : points(points), eps(eps) {
        for (size_t i = 0; i < points.size(); i++) {
            rtree.insert(std::make_pair(points[i], i));
        }
    }

    template <class InsertIterator>
    void operator()(const Point &p, InsertIterator it) const {
        Box box(Point(p.x() - eps, p.y() - eps),
                Point(p.x() + eps, p.y() + eps));
        for (Value &v : rtree | bgi::adaptors::queried(bgi::intersects(box))) {
            Point &q = v.first;
            if (bg::distance(p, q) <= eps)
                it = &points[v.second];
        }
    }
};

class SimpleRangeQuery {
    const std::vector<Point> &points;
    const double eps;

  public:
    SimpleRangeQuery(const std::vector<Point> &points, const double eps)
        : points(points), eps(eps) {}

    template <class InsertIterator>
    void operator()(const Point &p, InsertIterator it) const {
        for (const Point &q : points) {
            if (bg::distance(p, q) <= eps) {
                it = &q;
            }
        }
    }
};

template <class T, class D> class VecMap {
    const T *m_start;
    std::vector<D> m_data;

  public:
    VecMap(const std::vector<T> &vec) {
        m_start = vec.data();
        m_data = std::vector<D>(vec.size());
    }

    VecMap(const std::vector<T> &vec, const D &val) {
        m_start = vec.data();
        m_data = std::vector<D>(vec.size(), val);
    }

    D &operator[](const T *t) { return m_data[t - m_start]; }
    D &operator[](const T &t) { return m_data[&t - m_start]; }

    const std::vector<D> &data() { return m_data; }
};

template <class RangeQuery = SimpleRangeQuery> class OriginalDBSCAN : DBSCAN {
  public:
    using DBSCAN::DBSCAN;

    Result operator()(const std::vector<Point> &points) {
        VecMap<Point, PointData> point_data(points);
        RangeQuery range_query(points, eps);

        std::vector<const Point *> neighborhood;
        Id id = NOISE;
        for (const Point &p : points) {
            if (point_data[p].cluster != UNCLASSIFIED) {
                continue;
            }
            neighborhood.clear();
            range_query(p, std::back_inserter(neighborhood));
            if (neighborhood.size() < minpts) {
                // no core point
                point_data[p].cluster = NOISE;
            } else {
                // core point
                point_data[p].is_core = true;
                // start new cluster
                id++;
                std::stack<const Point *> seeds;
                for (const Point *q : neighborhood) {
                    point_data[q].cluster = id;
                    if (q != &p) {
                        seeds.push(q);
                    }
                }
                // expand cluster
                while (!seeds.empty()) {
                    const Point &p = *seeds.top();
                    // only removes the pointer
                    seeds.pop();
                    neighborhood.clear();
                    range_query(p, std::back_inserter(neighborhood));
                    if (neighborhood.size() < minpts) {
                        // border point
                        continue;
                    }
                    // else core point
                    point_data[p].is_core = true;
                    for (const Point *q : neighborhood) {
                        if (point_data[q].cluster == UNCLASSIFIED ||
                            point_data[q].cluster == NOISE) {
                            if (point_data[q].cluster == UNCLASSIFIED) {
                                // else border point
                                seeds.push(q);
                            }
                            point_data[q].cluster = id;
                        }
                    }
                }
            }
        }

        // return result
        return Result{point_data.data()};
    }
};

template <class Grid>
using OriginalDBSCANwGrid = OriginalDBSCAN<GridRangeQuery<Grid>>;

template <class Grid> class FasterDBSCAN : DBSCAN {
  public:
    using DBSCAN::DBSCAN;

    Result operator()(const std::vector<Point> &points) {
        // Construct grid
        Grid g(points, eps);

        // Initialize data
        VecMap<Point, PointData> point_data(points);
        VecMap<typename Grid::Cell, char> core_cell(g.cells());

        // Determine core points
        for (const typename Grid::Cell &c : g) {
            unsigned int c_size = c.size();
            if (c_size >= minpts) {
                for (const Point *p : c) {
                    point_data[p].is_core = true;
                }
                core_cell[c] = true;
            } else {
                for (const Point *p : c) {
                    unsigned int n_points = c_size;
                    for (const typename Grid::Cell *nc :
                         g.neighboring_cells(c)) {
                        for (const Point *q : *nc) {
                            if (bg::distance(p, q) <= eps) {
                                if (++n_points == minpts) {
                                    point_data[p].is_core = true;
                                    core_cell[c] = true;
                                    goto terminate;
                                }
                            }
                        }
                    }
                terminate:;
                }
            }
        }

        // Merge clusters
        Id id = 0;
        std::stack<const typename Grid::Cell *> stack;
        // process cells with core points
        for (const typename Grid::Cell &c : g) {
            if (!core_cell[c]) {
                continue;
            }
            // start new cluster
            id += 1;
            stack.push(&c);
            // mark cell as processed
            core_cell[c] = false;
            while (!stack.empty()) {
                const typename Grid::Cell &c = *stack.top();
                // only removes the pointer
                stack.pop();
                // process only core points
                for (const Point *p : c) {
                    if (!point_data[p].is_core) {
                        continue;
                    }
                    // assign point to cluster
                    point_data[p].cluster = id;
                    // process neighboring cells with core points
                    for (const typename Grid::Cell *nc :
                         g.neighboring_cells(c)) {
                        if (!core_cell[nc]) {
                            continue;
                        }
                        // process only core points
                        for (const Point *q : *nc) {
                            if (!point_data[q].is_core) {
                                continue;
                            }
                            // check if point belongs to the cluster
                            if (bg::distance(p, q) <= eps) {
                                point_data[q].cluster = id;
                                stack.push(nc);
                                core_cell[nc] = false;
                                break;
                            }
                        }
                    }
                }
            }
        }

        // Determine border points
        for (const typename Grid::Cell &c : g) {
            for (const Point *p : c) {
                if (point_data[p].is_core) {
                    continue;
                }
                // find nearest core point
                double min = eps;
                const Point *r = nullptr;
                for (const Point *q : c) {
                    if (!point_data[q].is_core) {
                        continue;
                    }
                    double d = bg::distance(p, q);
                    if (d <= min) {
                        min = d;
                        r = q;
                    }
                }
                for (const typename Grid::Cell *nc : g.neighboring_cells(c)) {
                    for (const Point *q : *nc) {
                        if (!point_data[q].is_core) {
                            continue;
                        }
                        double d = bg::distance(p, q);
                        if (d <= min) {
                            min = d;
                            r = q;
                        }
                    }
                }
                // assign point to cluster
                if (r != nullptr) {
                    point_data[p].cluster = point_data[r].cluster;
                } else {
                    point_data[p].cluster = NOISE;
                }
            }
        }
        // return result
        return Result{point_data.data()};
    }
};
