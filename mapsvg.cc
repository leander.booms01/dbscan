#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

// https://www.boost.org/
#include <boost/geometry/io/svg/svg_mapper.hpp>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include "dbscan.hh"

int main(int ac, char **av) {
    try {
        auto usage = "Usage: dbscan <eps> <minpts> [options]";
        po::options_description desc("Options");
        desc.add_options()("help", "Show help message")(
            "eps", po::value<double>(), "epsilon parameter")(
            "minpts", po::value<unsigned int>(), "minPts parameter")(
            "scale", po::value<double>(),
            "Output scale (default = 1.0)")("result", "Map result")(
            "graph", "Map graph")("nonregular", "Map nonregular grid");

        po::variables_map vm;
        po::store(po::command_line_parser(ac, av).options(desc).run(), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << usage << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        // read input
        std::vector<Point> points;
        CoordinateType x, y;
        while (std::cin >> x >> y) {
            points.push_back(Point(x, y));
        }

        // initialize boundaries
        auto [x_min_elem, x_max_elem] = std::minmax_element(
            points.begin(), points.end(),
            [](const Point &p, const Point &q) { return p.x() < q.x(); });
        auto [y_min_elem, y_max_elem] = std::minmax_element(
            points.begin(), points.end(),
            [](const Point &p, const Point &q) { return p.y() < q.y(); });
        const CoordinateType width = x_max_elem->x() - x_min_elem->x(),
                             height = y_max_elem->y() - y_min_elem->y();

        // initialize svg mapper
        double scale = 1.0;
        if (vm.count("scale")) {
            scale = vm["scale"].as<double>();
        }
        boost::geometry::svg_mapper<Point> mapper(std::cout, width * scale,
                                                  height * scale);

        for (Point &p : points) {
            mapper.add(p);
        }

        // map nonregular grid
        if (vm.count("nonregular")) {
            const double eps = vm["eps"].as<double>();

            // initialize dimensions
            auto [x_min_elem, x_max_elem] = std::minmax_element(
                points.begin(), points.end(),
                [](const Point &p, const Point &q) { return p.x() < q.x(); });
            auto [y_min_elem, y_max_elem] = std::minmax_element(
                points.begin(), points.end(),
                [](const Point &p, const Point &q) { return p.y() < q.y(); });
            const CoordinateType width = x_max_elem->x() - x_min_elem->x(),
                                 height = y_max_elem->y() - y_min_elem->y();

            NonregularGrid g(points, eps);
            for (NonregularGrid::Cell c : g) {
                mapper.map(c.bounds.box(),
                           "fill:none;stroke:rgb(0,0,0);stroke-width:0.5");
            }
        }

        // map graph
        if (vm.count("graph")) {
            const double eps = vm["eps"].as<double>();
            const unsigned int minpts = vm["minpts"].as<unsigned int>();

            RTreeRangeQuery range_query(points, eps);
            for (Point &p : points) {
                std::vector<const Point *> p_neighbors;
                range_query(p, std::back_inserter(p_neighbors));
                for (const Point *np : p_neighbors) {
                    if (&p < np)
                        continue;
                    std::vector<const Point *> np_neighbors;
                    range_query(*np, std::back_inserter(np_neighbors));

                    Segment edge(p, *np);
                    if (p_neighbors.size() >= minpts &&
                        np_neighbors.size() >= minpts) {
                        mapper.map(edge, "stroke:rgb(0,0,0);stroke-width:0.4");
                    } else {
                        mapper.map(edge, "stroke:rgb(0,0,0);stroke-width:0.4;"
                                         "stroke-dasharray:0.4,0.8");
                    }
                }
            }
        }

        // map points
        if (vm.count("eps") && vm.count("minpts")) {
            const double eps = vm["eps"].as<double>();
            const unsigned int minpts = vm["minpts"].as<unsigned int>();
            FasterDBSCAN<NonregularGrid> dbscan(eps, minpts);
            Result res = dbscan(points);

            size_t n = points.size();
            for (size_t i = 0; i < n; i++) {
                Point &p = points[i];
                PointData &pd = res.data[i];
                switch (pd.type()) {
                case TYPE_CORE_POINT:
                    mapper.map(p, "fill:rgb(0,0,0)", 1);
                    break;
                case TYPE_BORDER_POINT:
                    mapper.map(p,
                               "fill:rgb(255,255,255);stroke:rgb(0,0,0);stroke-"
                               "width:0.4",
                               0.8);
                    break;
                case TYPE_NOISE:
                    // mapper.map(p,
                    // "fill:rgb(255,255,255);stroke:rgb(0,0,0);stroke-width:0.4",
                    // 0.8);
                    mapper.map(p,
                               "fill:rgb(255,255,255);stroke:rgb(0,0,0);stroke-"
                               "width:0.4",
                               0.8);
                    break;
                }
            }
        } else {
            for (Point &p : points) {
                mapper.map(p, "fill:rgb(0,0,0)", 1);
            }
        }

        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
