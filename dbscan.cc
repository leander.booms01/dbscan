#include <chrono>

#include "dbscan.hh"

// https://www.boost.org/
#include <boost/program_options.hpp>

namespace po = boost::program_options;

template <class DBSCAN>
Result dbscan(const std::vector<Point> &points, const double eps,
              const unsigned int minpts) {
    DBSCAN dbscan_f(eps, minpts);
    return dbscan_f(points);
};

int main(int ac, char **av) {
    try {
        auto usage = "Usage: dbscan <eps> <minpts> [options]";
        po::options_description desc("Options");
        desc.add_options()("help", "Show help message")(
            "eps", po::value<double>(), "epsilon parameter")(
            "minpts", po::value<double>(),
            "minPts parameter")("original,o", "Original DBSCAN algorithm")(
            "faster,f", "Faster DBSCAN algorithm")(
            "nonregular,n", "Nonregular grid")("regular,r", "Regular grid")(
            "r-tree,t", "R*-Tree (only used with -o)")(
            "benchmark", po::value<unsigned int>(), "Benchmark mode");

        po::positional_options_description posopts;
        posopts.add("eps", 1);
        posopts.add("minpts", 1);

        po::variables_map vm;
        po::store(po::command_line_parser(ac, av)
                      .options(desc)
                      .positional(posopts)
                      .run(),
                  vm);
        po::notify(vm);

        if (vm.count("help") || !(vm.count("eps") && vm.count("minpts"))) {
            std::cout << usage << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        const double eps = vm["eps"].as<double>();
        const unsigned int minpts = vm["minpts"].as<double>();

        const bool original = vm.count("original");
        const bool faster = vm.count("faster");
        const bool regular = vm.count("regular");
        const bool nonregular = vm.count("nonregular");
        const bool r_tree = vm.count("r-tree");

        // read input
        std::vector<Point> points;
        CoordinateType x, y;
        while (std::cin >> x >> y) {
            points.push_back(Point(x, y));
        }
        size_t n = points.size();

        unsigned int runs = 1;
        if (vm.count("benchmark")) {
            runs = vm["benchmark"].as<unsigned int>();
        }

        Result res;
        double avg_time = 0;
        for (unsigned int run = 0; run < runs; run++) {
            // start clock
            auto start = std::chrono::high_resolution_clock::now();
            // run dbscan
            if (original) {
                if (nonregular) {
                    res = dbscan<OriginalDBSCANwGrid<NonregularGrid>>(
                        points, eps, minpts);
                } else if (regular) {
                    res = dbscan<OriginalDBSCANwGrid<RegularGrid>>(points, eps,
                                                                   minpts);
                } else if (r_tree) {
                    res = dbscan<OriginalDBSCAN<RTreeRangeQuery>>(points, eps,
                                                                  minpts);
                } else {
                    res = dbscan<OriginalDBSCAN<SimpleRangeQuery>>(points, eps,
                                                                   minpts);
                }
            } else if (faster) {
                if (nonregular) {
                    res = dbscan<FasterDBSCAN<NonregularGrid>>(points, eps,
                                                               minpts);
                } else if (regular) {
                    res =
                        dbscan<FasterDBSCAN<RegularGrid>>(points, eps, minpts);
                }
            }
            // stop clock
            auto end = std::chrono::high_resolution_clock::now();
            auto time = std::chrono::duration_cast<std::chrono::microseconds>(
                            end - start)
                            .count();
            avg_time += time * (1.0 / runs);
        }

        if (vm.count("benchmark")) {
            std::cout << avg_time << " ";
            return EXIT_SUCCESS;
        }

        // return result
        for (size_t i = 0; i < n; i++) {
            Point &p = points[i];
            PointData &pd = res.data[i];
            std::cout << p.x() << " " << p.y() << " " << pd.is_core << " "
                      << pd.cluster << std::endl;
        }

        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
