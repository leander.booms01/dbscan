#include <iostream>

// https://www.boost.org/
#include <boost/program_options.hpp>

namespace po = boost::program_options;

#include "dbscan.hh"

struct PointResult {
    Point point;
    PointData data;
};

int main(int ac, char **av) {
    try {
        auto usage = "Usage: resultcheck [options]";
        po::options_description desc("Options");
        desc.add_options()("help", "Show help message")(
            "ratio", po::value<double>(), "Ratio of noise");

        po::positional_options_description p;
        p.add("ratio", 1);

        po::variables_map vm;
        po::store(
            po::command_line_parser(ac, av).options(desc).positional(p).run(),
            vm);
        po::notify(vm);

        if (vm.count("help") || !(vm.count("ratio"))) {
            std::cout << usage << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        const double ratio = vm["ratio"].as<double>();

        // read points
        std::vector<PointResult> points;
        CoordinateType x, y;
        bool is_core;
        Id cluster;
        while (std::cin >> x >> y >> is_core >> cluster) {
            points.push_back(
                PointResult{Point(x, y), PointData(is_core, cluster)});
        }
        size_t n = points.size();

        /*
        size_t correct = 0;
        for (unsigned int i = 0; i < n; i++) {
            PointData &pd = points[i].data;
            if ((pd.cluster != 0) == (i >= ratio*n)) {
                correct++;
            }
        }
        */

        size_t n_noise = ratio * n;
        size_t n_cluster = n - n_noise;
        size_t correct_noise = 0;
        size_t correct_cluster = 0;
        for (unsigned int i = 0; i < n; i++) {
            PointData &pd = points[i].data;
            if (i < n_noise) {
                if (pd.cluster == 0)
                    correct_noise++;
            } else {
                if (pd.cluster != 0)
                    correct_cluster++;
            }
        }
        double correct_noise_ratio = correct_noise * (1.0 / n_noise);
        double correct_cluster_ratio = correct_cluster * (1.0 / n_cluster);
        double correct = correct_noise_ratio * correct_cluster_ratio;
        // double correct = pow(correct_noise_ratio, ratio) *
        // pow(correct_cluster_ratio, ratio);

        std::cout << correct << " ";

        return EXIT_SUCCESS;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}