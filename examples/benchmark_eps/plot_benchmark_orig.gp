input = ARG1

set terminal svg font ",16"
set output ARG2

set ylabel "time in microseconds"
set xlabel "eps"

plot input using 1:2 title "original" with linespoints pt 5 ps 0.4, \
     input using 1:3 title "original with nonregular grid" with linespoints pt 5 ps 0.4, \
     input using 1:4 title "original with regular grid" with linespoints pt 5 ps 0.4