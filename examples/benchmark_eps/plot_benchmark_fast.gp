input = ARG1

set terminal svg font ",16"
set output ARG2

set ylabel "time in microseconds"
set xlabel "eps"

plot input using 1:5 title "faster with nonregular grid" with linespoints pt 5 ps 0.4 lt 4, \
     input using 1:6 title "faster with regular grid" with linespoints pt 5 ps 0.4 lt 5