input = ARG1

set terminal svg font ",16"
set output ARG2

set ylabel "c"
set xlabel "eps"

plot input using 1:5 with lines title "minpts = 8"