#!/usr/bin/env Rscript

df <- read.table("data_eps.in")

for (i in 2:ncol(df)) {
    row = which.max(df[, i])
    minpts = (i-1)*2
    eps = df[row,1]
    c = df[row,i]
    cat(minpts, ' ', eps, ' ', c, '\n')
}
