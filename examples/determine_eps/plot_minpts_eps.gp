input = ARG1

set terminal svg font ",16"
set output ARG2

set xlabel "minpts"
set ylabel "eps"

set xtics 4

plot input using 1:2 with linespoints notitle pt 5 lt 1