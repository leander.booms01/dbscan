input = ARG1

set terminal svg font ",16"
set output ARG2

set ylabel "time in microseconds"
set xlabel "minpts"

set yrange [0:15000]
set xtics 4

plot input using 1:2 with linespoints title "original with nonregular grid" pt 5 ps 0.4,\
     input using 1:3 with linespoints title "original with regular grid" pt 5 ps 0.4,\
     input using 1:4 with linespoints title "faster with nonregular grid" pt 5 ps 0.4,\
     input using 1:5 with linespoints title "faster with regular grid" pt 5 ps 0.4