CXX      = g++
CXXFLAGS = -Wall -O2 -std=c++20

.PHONY: examples

all: random dbscan mapsvg resultcheck

random: random.o
	$(CXX) -o random random.o -lboost_program_options

dbscan.o: dbscan.hh

dbscan: dbscan.o
	$(CXX) -o dbscan dbscan.o -lboost_program_options

mapsvg: mapsvg.o
	$(CXX) -o mapsvg mapsvg.o -lboost_program_options

resultcheck: resultcheck.o
	$(CXX) -o resultcheck resultcheck.o -lboost_program_options

clean:
	$(RM) random random.o
	$(RM) dbscan dbscan.o
	$(RM) mapsvg mapsvg.o
	$(RM) resultcheck resultcheck.o

format:
	clang-format -i *.cc *.hh